package tests.solutionstests;

import driver.manager.TestBase;
import org.testng.Assert;
import org.testng.annotations.Test;
import pageobjects.MainPage;
import pageobjects.solutions.InternetRzeczyIDataSciencePage;

public class InternetRzeczyIDataScienceTests extends TestBase {

    @Test(description = "This test case will verify, whether correct page Header text is displayed, after reaching Internet Rzeczy I Data Science page")
    public void openInternetRzeczyIDataSciencePage() {
        MainPage mainPage = new MainPage();
        InternetRzeczyIDataSciencePage internetRzeczyIDataSciencePage =
                mainPage
                        .goToInternetRzeczyIDataSciencePage();
        Assert.assertEquals(internetRzeczyIDataSciencePage.assertHeader(), "Internet Rzeczy");
    }
}
