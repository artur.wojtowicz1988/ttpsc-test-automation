package tests.solutionstests;

import driver.manager.TestBase;
import org.testng.Assert;
import org.testng.annotations.Test;
import pageobjects.MainPage;
import pageobjects.solutions.RozszerzonaRzeczywistoscPage;

public class RozszerzonaRzeczywistoscTests extends TestBase {

    @Test(description = "This test case will verify, whether correct page Header text is displayed, after reaching Rozszerzona Rzeczywistosc page")
    public void openRozszerzonaRzeczywistoscPage() {
        MainPage mainPage = new MainPage();
        RozszerzonaRzeczywistoscPage rozszerzonaRzeczywistoscPage =
                mainPage
                        .goToRozszerzonaRzeczywistoscPage();
        Assert.assertEquals(rozszerzonaRzeczywistoscPage.assertTitleBanner(), "Augmented Reality");
    }
}
