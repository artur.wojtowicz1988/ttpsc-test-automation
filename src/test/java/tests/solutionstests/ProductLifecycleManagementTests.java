package tests.solutionstests;

import driver.manager.TestBase;
import org.testng.Assert;
import org.testng.annotations.Test;
import pageobjects.MainPage;
import pageobjects.solutions.ProductLifecycleManagementPage;


public class ProductLifecycleManagementTests extends TestBase {

    @Test(description = "This test case will verify, whether correct page Header text is displayed, after reaching Rozszerzona Rzeczywistosc page")
    public void openProductLifecycleManagementPage() {
        MainPage mainPage = new MainPage();
        ProductLifecycleManagementPage productLifecycleManagementPage =
                mainPage
                        .goToProductLifecycleManagementPage();
        Assert.assertEquals(productLifecycleManagementPage.assertTitleBanner(), "Product Lifecycle Management");
    }
}
