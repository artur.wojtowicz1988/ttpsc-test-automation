package tests;

import driver.manager.TestBase;
import org.testng.Assert;
import org.testng.annotations.Test;
import pageobjects.MainPage;
import pageobjects.kontakt.KontaktPage;

public class KontaktFormTests extends TestBase {

    @Test(description = "Example TC: This test case will verify if EndUser is able to send proper data through Kontakt form")
    public void sendCorrectDataThroughKontaktForm() throws InterruptedException {
        MainPage mainPage = new MainPage();
        KontaktPage kontaktPage =
                mainPage
                        .goToKontaktPage()
                        .fillInName("TestName")
                        .fillInEmail("test@test.pl")
                        .fillInPhoneNumber(123123123)
                        .fillInMessage("testMessage")
                        .checkPolicyrodo()
                        .checkNewsletter();
        Thread.sleep(2000);
        Assert.assertEquals(kontaktPage.getName(), "TestName");


    }
}
