package tests;

import driver.manager.TestBase;
import org.testng.Assert;
import org.testng.annotations.Test;
import pageobjects.CaseStudiesPage;
import pageobjects.MainPage;
import pageobjects.solutions.InternetRzeczyIDataSciencePage;

public class CaseStudiesTests extends TestBase {

    @Test(description = "This test case will verify, whether correct page Header text is displayed, after reaching Case Studies page")
    public void openCaseStudiesPage() {
        MainPage mainPage = new MainPage();
        CaseStudiesPage caseStudiesPage =
                mainPage
                        .goToCaseStudiesPage();
        Assert.assertEquals(caseStudiesPage.assertHeader(), "Case Studies");
    }
}
