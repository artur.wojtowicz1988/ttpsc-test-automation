package driver.manager;

public class DriverUtilities {

    public static void setInitialConfiguration() {
        DriverManager.getWebDriver().manage().window().maximize();

    }

    public static void navigateToPage(String URL) {
        DriverManager.getWebDriver().navigate().to(URL);
    }
}

