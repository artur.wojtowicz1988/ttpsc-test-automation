package driver.manager;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

public class TestBase {

    public WebDriver driver;

    @BeforeMethod
    public void setUp() {
        DriverManager.getWebDriver();
        DriverUtilities.setInitialConfiguration();
        DriverUtilities.navigateToPage("https://www.ttpsc.com/pl/");

    }

    @AfterMethod
    public void tearDown() {
        DriverManager.disposeDriver();
    }
}
