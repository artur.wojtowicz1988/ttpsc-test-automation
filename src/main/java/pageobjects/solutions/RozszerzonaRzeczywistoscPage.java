package pageobjects.solutions;

import driver.manager.DriverManager;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import waits.WaitForElement;

public class RozszerzonaRzeczywistoscPage {

    public RozszerzonaRzeczywistoscPage() {
        PageFactory.initElements(DriverManager.getWebDriver(), this);
    }

    @FindBy(css = "h1[class='title-banner line-center']")
    private WebElement titleBanner;

    public String assertTitleBanner(){
        WaitForElement.waitUntilElementsIsVisible(titleBanner);
        String getTextTitleBanner = titleBanner.getText();
        return getTextTitleBanner;
    }
}
