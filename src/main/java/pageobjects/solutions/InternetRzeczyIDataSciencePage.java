package pageobjects.solutions;

import driver.manager.DriverManager;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import waits.WaitForElement;

public class InternetRzeczyIDataSciencePage {

    public InternetRzeczyIDataSciencePage() {
        PageFactory.initElements(DriverManager.getWebDriver(), this);
    }

    @FindBy(css = "div[class='et_pb_text_inner']>h1")
    private WebElement pageHeader;

    public String assertHeader(){
        WaitForElement.waitUntilElementsIsVisible(pageHeader);
        String getTextHeader = pageHeader.getText();
        return getTextHeader;
    }
}
