package pageobjects.kontakt;

import driver.manager.DriverManager;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import waits.WaitForElement;

public class KontaktPage {

    public KontaktPage() {
        PageFactory.initElements(DriverManager.getWebDriver(), this);
    }

    @FindBy(css = "input[id='namefield']")
    private WebElement nameField;

    @FindBy(css = "input[id='email']")
    private WebElement emailField;

    @FindBy(css = "input[id='phone']")
    private WebElement phoneField;

    @FindBy(css = "textarea[class='wpcf7-form-control wpcf7-textarea move-placeholder message']")
    private WebElement messageField;

    @FindBy(css = "input[name='policyrodo']")
    private WebElement policyrdoCheckbox;

    @FindBy(css = "input[name='newsletter-agree']")
    private WebElement newsletterCheckbox;


    public KontaktPage fillInName(String name) {
        WaitForElement.waitUntilElementsIsClickable(nameField);
        nameField.sendKeys(name);
        return this;
    }

    public KontaktPage fillInEmail(String email) {
        WaitForElement.waitUntilElementsIsClickable(emailField);
        emailField.sendKeys(email);
        return this;
    }

    public KontaktPage fillInPhoneNumber(Integer phone) {
        WaitForElement.waitUntilElementsIsClickable(phoneField);
        phoneField.sendKeys(String.valueOf(phone));
        return this;
    }

    public KontaktPage fillInMessage(String message) {
        WaitForElement.waitUntilElementsIsClickable(messageField);
        messageField.sendKeys(message);
        return this;
    }

    public KontaktPage checkPolicyrodo() {
        WaitForElement.waitUntilElementsIsClickable(policyrdoCheckbox);
        policyrdoCheckbox.click();
        return this;
    }

    public KontaktPage checkNewsletter() {
        WaitForElement.waitUntilElementsIsClickable(newsletterCheckbox);
        newsletterCheckbox.click();
        return this;
    }

    public String getName() {
        String getName = nameField.getAttribute("value");
        return getName;
    }

}
