package pageobjects;

import driver.manager.DriverManager;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import waits.WaitForElement;

public class CaseStudiesPage {

    public CaseStudiesPage() {
        PageFactory.initElements(DriverManager.getWebDriver(), this);
    }

    @FindBy(css = "span[class='title-big']")
    private WebElement titleBig;

    public String assertHeader(){
        WaitForElement.waitUntilElementsIsVisible(titleBig);
        String getHeader = titleBig.getText();
        return getHeader;
    }
}
