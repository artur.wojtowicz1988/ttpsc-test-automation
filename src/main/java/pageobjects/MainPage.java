package pageobjects;

import driver.manager.DriverManager;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import pageobjects.kontakt.KontaktPage;
import pageobjects.solutions.InternetRzeczyIDataSciencePage;
import pageobjects.solutions.ProductLifecycleManagementPage;
import pageobjects.solutions.RozszerzonaRzeczywistoscPage;
import waits.WaitForElement;

public class MainPage {

    public MainPage(){
        PageFactory.initElements(DriverManager.getWebDriver(), this);
    }

    @FindBy(css = "li[id='menu-item-6325']")
    private WebElement solutionsPage;

    @FindBy(css = "li[id='menu-item-116']")
    private WebElement caseStudiesPage;

    @FindBy(css = "li[id='menu-item-33315']")
    private WebElement webinaryPage;

    @FindBy(css = "li[id='menu-item-33262']")
    private WebElement partnerzyPage;

    @FindBy(css = "li[id='menu-item-4068']")
    private WebElement blogPage;

    @FindBy(css = "li[id='menu-item-6350']")
    private WebElement karieraPage;

    @FindBy(css = "li[id='menu-item-24753']")
    private WebElement kontaktPage;

    @FindBy(css = "li[id='menu-item-6327']")
    private WebElement internetRzeczyIDataSciencePage;

    @FindBy(css = "li[id='menu-item-6328']")
    private WebElement rozszerzonaRzeczywistoscPage;

    @FindBy(css = "li[id='menu-item-6329']")
    private WebElement productLifecycleManagementPage;

    public CaseStudiesPage goToCaseStudiesPage(){
        WaitForElement.waitUntilElementsIsClickable(caseStudiesPage);
        caseStudiesPage.click();
        return new CaseStudiesPage();
    }

    public void goToWebinaryPage(){
        webinaryPage.click();
    }

    public void goToPartnerzyPage(){
        partnerzyPage.click();
    }

    public void goToBlogPage(){
        blogPage.click();
    }

    public void goToKarieraPage(){
        karieraPage.click();
    }

    public KontaktPage goToKontaktPage(){
        WaitForElement.waitUntilElementsIsClickable(kontaktPage);
        kontaktPage.click();
        return new KontaktPage();
    }

    public InternetRzeczyIDataSciencePage goToInternetRzeczyIDataSciencePage(){
        Actions action = new Actions(DriverManager.getWebDriver());
        WaitForElement.waitUntilElementsIsVisible(solutionsPage);
        action.moveToElement(solutionsPage)
                .perform();

        WaitForElement.waitUntilElementsIsClickable(internetRzeczyIDataSciencePage);
        internetRzeczyIDataSciencePage.click();
        return new InternetRzeczyIDataSciencePage();
    }

    public RozszerzonaRzeczywistoscPage goToRozszerzonaRzeczywistoscPage(){
        Actions action = new Actions(DriverManager.getWebDriver());
        WaitForElement.waitUntilElementsIsVisible(solutionsPage);
        action.moveToElement(solutionsPage)
                .perform();

        WaitForElement.waitUntilElementsIsClickable(rozszerzonaRzeczywistoscPage);
        rozszerzonaRzeczywistoscPage.click();
        return new RozszerzonaRzeczywistoscPage();
    }

    public ProductLifecycleManagementPage goToProductLifecycleManagementPage(){
        Actions action = new Actions(DriverManager.getWebDriver());
        WaitForElement.waitUntilElementsIsVisible(solutionsPage);
        action.moveToElement(solutionsPage)
                .perform();

        WaitForElement.waitUntilElementsIsClickable(productLifecycleManagementPage);
        productLifecycleManagementPage.click();
        return new ProductLifecycleManagementPage();
    }
}
